0.0.5

* Rename `imap` to `($.$)`
* Add `igets` to `IxMonadState`

0.0.4

* Add indexed type-classes
* Add instances

0.0.3

* Relax `semigroupoids` bounds

0.0.2

* Relax `lens` bounds

0.0.1

* This change log starts
