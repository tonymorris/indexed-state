{-# OPTIONS_GHC -Wall #-}
{-# OPTIONS_GHC -Wno-dodgy-exports #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Control.Monad.State.Profunctor.Class(
  IxFunctor(..)
, IxApply(..)
, IxApplicative(..)
, IxBind (..)
, IxMonad(..)
, IxAlt(..)
, IxAlternative(..)
, IxMonadState(..)
, IxMonadFix(..)
, IxMonadCont(..)
, IxBindTrans(..)
, IxMonadTrans(..)
) where

import Control.Applicative ( Applicative(pure) )
import Control.Monad ( Monad )
import Data.Functor.Bind ( Bind )

class IxFunctor f where
  ($.$) ::
    (a -> b)
    -> f j k a
    -> f j k b

infixl 4 $.$

class IxFunctor f => IxApply f where
  (<*.*>) ::
    f j k (a -> b)
    -> f i j a
    -> f i k b

infixl 4 <*.*>

class IxApply f => IxApplicative f where
  ipure ::
    a
    -> f i i a

class IxApply f => IxBind f where
  (-<<<) ::
    (a -> f i j b)
    -> f j k a
    -> f i k b
  f -<<< g =
    g >>>- f
  (>>>-) ::
    f j k a
    -> (a -> f i j b)
    -> f i k b
  f >>>- g =
    g -<<< f
  {-# MINIMAL (-<<<) | (>>>-) #-}

infixr 1 -<<<
infixr 1 >>>-

class (IxApplicative f, IxBind f) => IxMonad f where

class IxFunctor f => IxAlt f where
  (<|.|>) ::
    f i j a
    -> f i j a
    -> f i j a

infixl 3 <|.|>

class IxAlt f => IxAlternative f where
  iempty ::
    f i j a

class IxMonad f => IxMonadState f where
  iget ::
    f t t t
  iget =
    istate (\s -> (s, s))
  iput ::
    t
    -> f t s ()
  iput s =
    istate (pure ((), s))
  istate ::
    (s -> (a, t))
    -> f t s a
  istate f =
    iget >>>- \t ->
    let ~(a, s') = f t
    in iput s' >>>-
    pure (ipure a)
  imodify ::
    (s -> t)
    -> f t s ()
  imodify f =
    istate (\s -> ((), f s))
  igets ::
    (s -> t)
    -> f s s t
  igets f =
    f $.$ iget
  {-# MINIMAL istate | iget, iput #-}

class IxMonadFix f where
  imfix ::
    (a -> f t s a)
    -> f t s a

class IxMonadCont f where
  icallCC ::
    ((a -> f t r b) -> f r s a)
    -> f r s a

class IxBindTrans g where
  iliftB ::
    Bind f =>
    f a
    -> g f s s a

class IxMonadTrans g where
  ilift ::
    Monad f =>
    f a
    -> g f s s a
