{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Control.Monad.State.Profunctor.IxState(
  IxStateT(..)
, IxState
, ixState
, swap
, module I
) where

import Control.Applicative
    ( Applicative((<*>), pure), Alternative((<|>), empty) )
import Control.Arrow
    ( Arrow(arr, first), ArrowApply(..), ArrowChoice(left) )
import Control.Category ( Category(..) )
import Control.Lens
    ( Choice(..),
      Profunctor(dimap),
      view,
      iso,
      over,
      _Wrapped,
      Field1(_1),
      Field2(_2),
      Iso,
      Rewrapped,
      Wrapped(..) )
import Control.Monad
    ( Monad(return, (>>=)), Functor(..), (>=>) )
import Control.Monad.Cont ( MonadCont(..) )
import Control.Monad.Error.Class ( MonadError(..) )
import Control.Monad.Fix ( MonadFix(..) )
import Control.Monad.IO.Class ( MonadIO(..) )
import Control.Monad.Reader.Class ( MonadReader(..) )
import Control.Monad.State.Class ( MonadState(..) )
import Control.Monad.State.Profunctor.Class as I
    ( IxMonadState(iget, iput, istate),
      IxAlternative(..),
      IxAlt(..),
      IxMonad,
      IxBind(..),
      IxApplicative(..),
      IxApply(..),
      IxFunctor(..),
      IxMonadFix(..),
      IxMonadCont(..),
      IxBindTrans(..),
      IxMonadTrans(..) )
import Control.Monad.Writer.Class
    ( MonadWriter(writer, listen, pass) )
import Data.Either ( Either(Right, Left), either )
import Data.Functor.Alt ( Alt((<!>)) )
import Data.Functor.Apply ( Apply(liftF2, (<.>)) )
import Data.Functor.Bind ( Bind((>>-)), (->-) )
import Data.Functor.Identity as I ( Identity(..) )
import Data.Monoid ( (<>), Monoid(mempty) )
import Data.Profunctor as I ( Strong(..) )
import Data.Semigroup ( Semigroup )
import Data.Semigroupoid ( Semigroupoid(..) )

newtype IxStateT f t s a =
  IxStateT (s -> f (a, t))

type IxState t s a =
  IxStateT Identity t s a

ixState ::
  Iso
    (IxState t s a)
    (IxState t' s' a')
    (s -> (a, t))
    (s' -> (a', t'))
ixState =
  iso
    (\(IxStateT f) -> runIdentity . f)
    (\f -> IxStateT (Identity . f))

swap ::
  (Functor f, Functor f') =>
  Iso
    (IxStateT f t s a)
    (IxStateT f' t' s' a')
    (IxStateT f a s t)
    (IxStateT f' a' s' t')
swap =
  iso
    (over _Wrapped (\f s -> fmap (\(a, t) -> (t, a)) (f s)))
    (over _Wrapped (\f s -> fmap (\(t, a) -> (a, t)) (f s)))

instance IxStateT f t s a ~ x =>
  Rewrapped (IxStateT f' t' s' a') x

instance Wrapped (IxStateT f t s a) where
  type Unwrapped (IxStateT f t s a) =
    s -> f (a, t)
  _Wrapped' =
    iso (\(IxStateT x) -> x) IxStateT

instance (Apply f, Semigroup a, Semigroup t) => Semigroup (IxStateT f t s a) where
  IxStateT f <> IxStateT g =
    IxStateT (\s -> liftF2 (<>) (f s) (g s))

instance (Applicative f, Apply f, Monoid a, Monoid t) => Monoid (IxStateT f t s a) where
  mempty =
    IxStateT (pure (pure (mempty, mempty)))

instance Functor f => Functor (IxStateT f t s) where
  fmap =
    ($.$)

instance Functor f => IxFunctor (IxStateT f) where
  f $.$ IxStateT g =
    IxStateT (fmap (over _1 f) . g)

instance Monad f => Apply (IxStateT f s s) where
  (<.>) =
    (<*.*>)

instance Monad f => IxApply (IxStateT f) where
  f <*.*> a =
    (-<<<) ($.$ a) f

instance Monad f => Applicative (IxStateT f s s) where
  pure =
    ipure
  (<*>) =
    (<*.*>)

instance Monad f => IxApplicative (IxStateT f) where
  ipure a =
    IxStateT (\s -> pure (a, s))

instance Monad f => Bind (IxStateT f s s) where
  (>>-) =
    (>>>-)

instance Monad f => IxBind (IxStateT f) where
  g -<<< (IxStateT f) =
    IxStateT (f >=> (\ ~(a', t') -> view _Wrapped (g a') t'))

instance Monad f => Monad (IxStateT f s s) where
  return =
    ipure
  (>>=) =
    (>>>-)

instance Monad f => IxMonad (IxStateT f) where

instance Alt f => Alt (IxStateT f s s) where
  (<!>) =
    (<|.|>)

instance Alt f => IxAlt (IxStateT f) where
  IxStateT f <|.|> IxStateT g =
    IxStateT (\s -> f s <!> g s)

instance (Alternative f, Monad f) => Alternative (IxStateT f s s) where
  IxStateT f <|> IxStateT g =
    IxStateT (\s -> f s <|> g s)
  empty =
    IxStateT (pure empty)

instance (Alt f, Alternative f) => IxAlternative (IxStateT f) where
  iempty =
    IxStateT (pure empty)

instance Monad f => MonadState s (IxStateT f s s) where
  state =
    istate
  get =
    iget
  put =
    iput

instance Monad f => IxMonadState (IxStateT f) where
  iget =
    IxStateT (\s -> pure (s, s))
  iput a =
    IxStateT (\_ -> pure ((), a))
  istate f =
    IxStateT (pure . f)

instance MonadReader r f => MonadReader r (IxStateT f s s) where
  ask =
    ilift ask
  reader =
    ilift . reader
  local f (IxStateT g) =
    IxStateT (local f . g)

instance MonadWriter r f => MonadWriter r (IxStateT f s s) where
  writer =
    ilift . writer
  listen =
    over _Wrapped (\f s -> fmap (\((a, t), r) -> ((a, r), t)) (listen (f s)))
  pass =
    over _Wrapped (\f s -> pass (fmap (\((a, k), t) -> ((a, t), k)) (f s)))

instance MonadError r f => MonadError r (IxStateT f s s) where
  throwError =
    ilift . throwError
  catchError (IxStateT f) g =
    IxStateT (\s -> catchError (f s) (\r -> view _Wrapped (g r) s))

instance MonadFix f => MonadFix (IxStateT f s s) where
  mfix =
    imfix

instance MonadFix f => IxMonadFix (IxStateT f) where
  imfix f =
    IxStateT (\s -> mfix (\ ~(a, _) -> view _Wrapped (f a) s))

instance MonadCont f => MonadCont (IxStateT f s s) where
  callCC f =
    IxStateT (\s -> callCC (\k -> view _Wrapped (f (\a -> IxStateT (\s' -> k (a, s')))) s))

instance MonadCont f => IxMonadCont (IxStateT f) where
  icallCC f =
    IxStateT (\s -> callCC (\k -> view _Wrapped (f (\a -> IxStateT (\s' -> k (a, s')))) s))

instance MonadIO f => MonadIO (IxStateT f s s) where
  liftIO =
    ilift . liftIO

instance (Semigroup t, Bind f) => Semigroupoid (IxStateT f t) where
  IxStateT f `o` IxStateT g =
    IxStateT (g ->- (\ ~(a, t) -> fmap (over _2 (t <>)) (f a)))

instance (Monoid t, Monad f) => Category (IxStateT f t) where
  IxStateT f . IxStateT g =
    IxStateT (g >=> (\ ~(a, t) -> fmap (over _2 (t <>)) (f a)))
  id =
    IxStateT (\s -> pure (s, mempty))

instance Functor f => Profunctor (IxStateT f t) where
  dimap f g (IxStateT k) =
    IxStateT (fmap (over _1 g) . k . f)

instance (Monoid t, Applicative f) => Choice (IxStateT f t) where
  left' =
    over _Wrapped (\f -> either (fmap (over _1 Left) . f) (\c -> pure (Right c, mempty)))
  right' =
    over _Wrapped (\f -> either (\c -> pure (Left c, mempty)) (fmap (over _1 Right) . f))

instance Functor f => Strong (IxStateT f t) where
  first' =
    over _Wrapped (\f (a, c) -> fmap (\(b, t) -> ((b, c), t)) (f a))
  second' =
    over _Wrapped (\f (c, a) -> fmap (\(b, t) -> ((c, b), t)) (f a))

instance (Monad f, Monoid t) => Arrow (IxStateT f t) where
  arr f =
    IxStateT (\s -> pure (f s, mempty))
  first =
    over _Wrapped (\f (b, d) -> fmap (\(c, t) -> ((c, d), t)) (f b))

instance (Monad f, Monoid t) => ArrowApply (IxStateT f t) where
  app =
    IxStateT (\(IxStateT k, b) -> k b)

instance (Monad f, Monoid t) => ArrowChoice (IxStateT f t) where
  left =
    over _Wrapped (\k -> either (fmap (over _1 Left) . k) (\d -> pure (Right d, mempty)))

instance IxBindTrans IxStateT where
  iliftB a =
    IxStateT (\s -> fmap (, s) a)

instance IxMonadTrans IxStateT where
  ilift a =
    IxStateT (\s -> fmap (, s) a)
